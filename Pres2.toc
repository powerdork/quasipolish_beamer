\beamer@endinputifotherversion {3.33pt}
\select@language {italian}
\select@language {italian}
\beamer@sectionintoc {2}{Domini}{13}{0}{1}
\beamer@subsectionintoc {2}{1}{Definizione}{13}{0}{1}
\beamer@subsectionintoc {2}{2}{Topologia}{21}{0}{1}
\beamer@subsectionintoc {2}{3}{Sobriet\IeC {\`a}}{23}{0}{1}
\beamer@sectionintoc {3}{Spazi polacchi}{26}{0}{2}
\beamer@subsectionintoc {3}{1}{Definizione}{26}{0}{2}
\beamer@subsectionintoc {3}{2}{Sobriet\IeC {\`a}}{33}{0}{2}
\beamer@subsectionintoc {3}{3}{Gerarchia boreliana}{38}{0}{2}
\beamer@sectionintoc {4}{Spazi quasi-polacchi}{41}{0}{3}
\beamer@subsectionintoc {4}{1}{Gerarchia generalizzata}{41}{0}{3}
\beamer@subsectionintoc {4}{2}{Gli spazi polacchi}{45}{0}{3}
\beamer@subsectionintoc {4}{3}{I domini $\omega $-continui}{47}{0}{3}
