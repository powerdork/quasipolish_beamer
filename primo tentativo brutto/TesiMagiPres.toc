\beamer@endinputifotherversion {3.33pt}
\select@language {italian}
\select@language {italian}
\beamer@sectionintoc {2}{Nozioni preliminari}{7}{0}{1}
\beamer@subsectionintoc {2}{1}{Topologie e metriche}{7}{0}{1}
\beamer@subsectionintoc {2}{2}{Alberi e ordini parziali}{11}{0}{1}
\beamer@sectionintoc {3}{Teorie gi\IeC {\`a} sviluppate}{13}{0}{2}
\beamer@subsectionintoc {3}{1}{I domini}{13}{0}{2}
\beamer@subsectionintoc {3}{2}{Gli spazi polacchi}{30}{0}{2}
\beamer@sectionintoc {4}{La novit\IeC {\`a}: i quasi-polacchi.}{51}{0}{3}
\beamer@subsectionintoc {4}{1}{La quasi-metrica}{51}{0}{3}
\beamer@subsectionintoc {4}{2}{Una ``nuova'' gerarchia}{52}{0}{3}
\beamer@subsectionintoc {4}{3}{(I sottospazi quasi-polacchi)}{53}{0}{3}
\beamer@subsectionintoc {4}{4}{Generalizzazione polacchi e domini}{54}{0}{3}
\beamer@sectionintoc {6}{ciccio pasticcio}{57}{1}{4}
\beamer@subsectionintoc {6}{1}{mandi}{57}{1}{4}
